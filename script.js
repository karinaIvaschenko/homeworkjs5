function createNewUser() {
    const userFirstName = prompt('Please, enter your name');
    const userLastName = prompt('Please, enter your surname');
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        }
    };
    return newUser;
}
const user = createNewUser();
console.log(user.getLogin());


